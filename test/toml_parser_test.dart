import '../lib/toml.dart';
import '../packages/unittest/unittest.dart';
import 'dart:io';
import 'dart:collection';

void main(){ 

  String easyone = 'resources/easyone.toml';
  String hardone = 'resources/hardone.toml';
  String stringsource = '';
  
  group('Creation', () {
    test('No input on constructor', () => expect(new TomlParser(), isNotNull));
  });
  
  group('Single line parsing', () {
    TomlParser p;
    LinkedHashMap<String,Object> result;
    setUp(() {p = new TomlParser();});
    
    test('Parse an empty line', (){
      result = p.parse([""]);
      expect(result, isNotNull);
      expect(result.isEmpty, equals(true));
      
      result = p.parse(null);
      expect(result, isNotNull);
      expect(result.isEmpty, equals(true));
      
      result = p.parse([" "]);
      expect(result, isNotNull);
      expect(result.isEmpty, equals(true));
    }
    );
    
    test('Parse a comment', (){
        result = p.parse(["# this is a comment"]);
        expect(result, isNotNull);
        expect(result.isEmpty, equals(true));
      }
    );
    
    test('Parse a group', (){
        result = p.parse(["[this.is.a.group]"]);
        expect(result, isNotNull);
        expect(result.isEmpty, equals(false));
        
        var groupThis = result["this"];
        
        expect(groupThis, isNotNull);
        expect(groupThis.length, equals(1));
        
        var groupIs = groupThis["is"] as LinkedHashMap;
        
        expect(groupIs, isNotNull);
        expect(groupIs.length, equals(1));
        
        var groupA = groupIs["a"] as LinkedHashMap;
        
        expect(groupA, isNotNull);
        expect(groupA.length, equals(1));
        
        var groupGroup = groupA["group"] as LinkedHashMap;
        
        expect(groupGroup, isNotNull);
        expect(groupGroup.length, equals(0));
      }
    );
    
    test('Parse a key-value line, value is string', (){
        result = p.parse(['line = "this is a string value"']);
        expect(result, isNotNull);
        expect(result.isEmpty, equals(false));
        expect(result["line"], isNotNull);
        expect(result["line"], equals("this is a string value"));
      }
    );
    
    // TODO this one returns a "fake" integer, return a real one
    test('Parse a key-value line, value is an integer', (){
        result = p.parse(['line = 123']);
        expect(result, isNotNull);
        expect(result.isEmpty, equals(false));
        expect(result["line"], isNotNull);
        expect(result["line"], equals("123"));
      }
    );
    
    // TODO this one returns a "fake" float, return a real one
    test('Parse a key-value line, value is a float', (){
        result = p.parse(['line = 2.22']);
        expect(result, isNotNull);
        expect(result.isEmpty, equals(false));
        expect(result["line"], isNotNull);
        expect(result["line"], equals("2.22"));
      }
    );
    
    // TODO this one returns a "fake" bool, return a real one
    test('Parse a key-value line, value is a boolean', (){
        result = p.parse(['line = true']);
        expect(result, isNotNull);
        expect(result.isEmpty, equals(false));
        expect(result["line"], isNotNull);
        expect(result["line"], equals("true"));
      }
    );
    
    // TODO this one returns a "fake" array, return a real one
    test('Parse a key-value line, value is an array', (){
        result = p.parse(['line = [an, array, of, values]']);
        expect(result, isNotNull);
        expect(result.isEmpty, equals(false));
        expect(result["line"], isNotNull);
        expect(result["line"], equals("an, array, of, values"));
      }
    );
    
    test('Parse a key-value, value is a string, with a comment', (){
        result = p.parse(['line = "having a comment" # this is a comment']);
        expect(result, isNotNull);
        expect(result.isEmpty, equals(false));
        expect(result["line"], isNotNull);
      }
    );
    
  });
  
  group('Unsupported formats', (){
    
    TomlParser p;
    setUp(() {p = new TomlParser();});
    
    test("Wrong comment format", () {  
      expect(() => p.parse(["a comment # line"]), throws);  
    });
    
    test("Wrong group format", () {  
      expect(() => p.parse(["[group wrong format]"]), throws);  
    });
    
    test("Wrong key-value format: dotted key", () {  
      expect(() => p.parse(["a.key = value"]), throws);  
    });
    
    test("Wrong key-value format: spaced key", () {  
      expect(() => p.parse(["a key = value"]), throws);  
    });
  });
}