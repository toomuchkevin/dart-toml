import '../lib/toml.dart';

// r"\[((\w+)(\.(\w+))+)\]
void main() {
  var source = "key = [8001, 8001, 8002]";
  var pattern = r"^\s*(\w+)\s*=\s*\[\n*\s*\n*((\w+)(\,\n*\s*(\w+))+)\n*\s*\n*]";
  var re = new RegExp(pattern,multiLine:true);
  Match m = re.firstMatch(source);
  
  print("${m}");
  print("0 = ${m[0]}");
  print("1 = ${m[1]}");
  print("2 = ${m[2]}");
}
