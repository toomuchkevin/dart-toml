TOML for Dart
===========

TOML for Dart is a library to parse and manage the [TOML markup language][toml-on-github] 
from Tom Preson-Werner ([@mojombo][mojombo]).

To know something more about the released version have a look at the
[CHANGELOG][changelog] to find the version that works best for you.

[![Build Status](https://drone.io/bitbucket.org/toomuchkevin/dart-toml/status.png)](https://drone.io/bitbucket.org/toomuchkevin/dart-toml/latest)

## Import

import "https://bitbucket.org/toomuchkevin/dart-toml"

## Getting started
TOML spec requires UTF-8 encoding. So input is intended to be UTF-8 formatted.

## Documentation
Not available yet. Please, be patient. :)

## Contribute

Feel free to report bugs and patches or ask me for improvements using BitBuckets's pull requests system on
[toomuchkevin/dart-toml][bb-pullrequest] or the [issue system][bb-issue]. Any feedback would be appreciated!

[changelog]: 
[toml-on-github]: https://github.com/mojombo/toml
[mojombo]: https://github.com/mojombo/
[bb-pullrequest]: https://bitbucket.org/toomuchkevin/dart-toml/pull-requests
[bb-issue]: https://bitbucket.org/toomuchkevin/dart-toml/issues?status=new&status=open