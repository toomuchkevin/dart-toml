import '../lib/toml.dart';
import '../packages/unittest/unittest.dart';
import 'dart:io';

void main(){ 

  String easyone = 'resources/easyone.toml';
  String hardone = 'resources/hardone.toml';
  String stringsource = '';
  
  group('Creation', () {
    test('empty input', () => expect(new Toml(""), isNotNull));
    test('empty path', () => expect(new Toml.fromFile(""), isNotNull));
  });
}