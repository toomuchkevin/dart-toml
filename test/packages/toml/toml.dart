/**
 * A simple TOML management library.
 */
library toml;

import 'dart:io';
import 'dart:json' as JSON;
import 'dart:utf';
import 'dart:collection';

part 'src/toml_parser.dart';

/**
 * The TOML class implementing all tools to load data from input string or file.
 * Input files are supposed to be in UTF-8 format.
 * 
 * If you are not familiar with TOML, just have a look here:
 * https://github.com/mojombo/toml.
 */
class Toml {
  
  /**
   * Create a new instance from the input String [tomlSource].
   */
  Toml(String tomlSource){
    // TODO
  }
  
  /**
   * Create a new instance parsing data from the input file [source]
   * if existing.
   */
  Toml.fromFile(String source){
    // TODO
  }
  
}

/**
 * A parser implementation for TOML markup language, based on
 * regular expressions.
 */
class TomlParser {
  
  final int _NEWLINE = "\n".codeUnits[0];
  final int _RETURNCARRIAGE = "\r".codeUnits[0];
  
  /*** comment line regular expression */
  var comment_re = new RegExp(r"(^#.*)");
  /*** group declaration line regular expression */
  var group_re = new RegExp(r"\[((\w+)(\.(\w+))+)\]");
  /*** key value having value as string declaration regular expression */
  var key_value_re = new RegExp(r'\s*(\w+)\s*=\s*');
  /*** key value having value as string declaration regular expression */
  var key_string_re = new RegExp(r'^\s*(\w+)\s*=\s*\"(.*)\"');
  /*** key value having value as integer regular expression */
  var key_int_re = new RegExp(r'^\s*(\w+)\s*=\s*(\d+)');
  /*** key value having value as double regular expression */
  var key_double_re = new RegExp(r'^\s*(\w+)\s*=\s*([-+]?\d*\.\d+([eE][-+]?\d+)?)');
  /*** key value having value as bool regular expression */
  var key_bool_re = new RegExp(r'^\s*(\w+)\s*=\s*(true|false)');
  /*** key value having value as an array regular expression */
  var key_array_re = new RegExp(r'^\s*(\w+)\s*=\s*\[\n*\s*\n*((\w+)(\,\n*\s*(\w+))+)\n*\s*\n*]');
  /*** key value having value as a date regular expression */
  var key_date_re = new RegExp(r"^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}Z");
  
  /*** The root element of the TOML instance */
  LinkedHashMap<String,Object> _root;
  
  /*** Exceptions enable / disable flag */
  bool _skipExceptions = false;
  
  /*** List of errors collected while parsing the input source */
  var _errors = [];
  
  /** 
   * Create and initialize a new parser instance,
   * eventually choosing to enable / disable exceptions. 
   */
  TomlParser([bool skipExceptions = false]){
    _root = new LinkedHashMap();
    _skipExceptions = skipExceptions;
  }
  
  /**
   * Loops through the input [sourceList] of strings, parsing
   * each one and managing comments, groups and key-value pairs.
   * Input strings are intended to be UTF-8 encoded.
   */
  LinkedHashMap<String,Object> parse(List<String> sourceList){
    
    if(sourceList == null){
      return _root;
    }
    
    if(sourceList.isEmpty){
      return _root;
    }
    
    // the very first group to work on is the root one
    LinkedHashMap<String,Object> currentGroup = _root;
    
    for(String source in sourceList){
    
      // empty line? skip it..
      if(source == null)
        continue;
      
      // remove white spaces
      source = source.trim();
    
      // empty line? skip it..
      if(source.isEmpty){
        continue;
      }
    
      // manage newline lines, skip them
      if(source.codeUnits[0] == _NEWLINE || source.codeUnits[0] == _RETURNCARRIAGE){
        continue;
      }
    
      if(_isComment(source)){ // line is a comment? skip it..
        continue;
        
      } else if(_isGroup(source)){ // line is a group? manage it..
        currentGroup = _getGroup(_getGroupDeclaration(source));
        
      } else if(_isKeyValue(source)) { // line is key value? parse it..
        _loadKeyValue(_parseKeyValue(source), currentGroup);
        
      } else { // mmm unknown format!
        _error("Format not recognized for input line: ${source}. This one seems not to be a comment, nor a group declaration, nor a key-value pair. Have a check on the input source.");
      }
    }
    
    return _root;
  }

  /**
   * Load input [keyvalue] values into the input [group].
   */
  _loadKeyValue(List<Object> keyvalue, LinkedHashMap<String,Object> group) {
    // this one is useful in case exceptions are disabled
    if(keyvalue == null || keyvalue.isEmpty){
      // nothing to do so far..
      return;
    }
    
    group[keyvalue[0]] = keyvalue[1];
  }

  /**
   * Get a List of 2 elements containing key and value
   * out of the input [source] string.
   * Key-value are parsed using regular expressions.
   */
  List<Object> _parseKeyValue(String source) {
    
    var result = new List<Object>(2);
    
    Match m;
    
    // matches which one?
    // TODO move m[2] parsing, what if m is null or empty?!
    
    if(key_string_re.hasMatch(source)){
      m = key_string_re.firstMatch(source);
      //result[1] = m[2];
    } else if(key_bool_re.hasMatch(source)){
      m = key_bool_re.firstMatch(source);
      //result[1] = _parseBool(m[2]);
    } else if(key_double_re.hasMatch(source)){
      m = key_double_re.firstMatch(source);
      //result[1] = _parseDouble(m[2]);
    } else if(key_int_re.hasMatch(source)){
      m = key_int_re.firstMatch(source);
      //result[1] = _parseInt(m[2]);
    } else if(key_array_re.hasMatch(source)){ // high level matching
      m = key_array_re.firstMatch(source);
      //result[1] = _parseArray(m[2]);
    } else if(key_date_re.hasMatch(source)){
      m = key_date_re.firstMatch(source);
      //result[1] = _parseDate(m[2]);
    }
    
    if(m == null){      
      _error("Format not recognized for input line: ${source}. Expected a key-value pair but the format is not supported.");
    }
    
    if(m[1] == null || m[1].isEmpty){      
      _error("Format not recognized for input line: ${source}. Expected a key-value pair but key seems to be empty.");
    }
    
    result[0] = m[1];
    result[1] = m[2]; // needs some improvements..
    
    return result;
  }
  
  bool _parseBool(String value){
    if(value == "false"){
      return false;
    }
    if(value == "true"){
      return true;
    }
    
    return false;
  }
  
  _parseDouble(String value){
    return value;
  }

  _parseInt(String value){
    return value;
  }
  
  _parseArray(String value){
    return value;
  }
  
  DateTime _parseDate(String value){
    return DateTime.parse(value);
  }

  /**
   * Load or create a group out of the input
   * [source] string.
   */
  LinkedHashMap<String,Object> _getGroup(String source) {
        
    List<String> groups = source.split(".");
    
    // first group to look into is the root one
    var currentGroup = _root;
    
    for(String groupName in groups){
      
      // current group does not exist, create it
      if(currentGroup.containsKey(groupName)){
        
        Object group = currentGroup[groupName];
        
        if(group is LinkedHashMap){
          
          currentGroup = group as LinkedHashMap;
          
        } else {
          _error("Action not supported for line ${source}: it's impossible to create a group having the same name as a property.");
        }
      } else {

        currentGroup[groupName] = new LinkedHashMap<String,Object>();
        currentGroup = currentGroup[groupName];
      }
    }
    
    return currentGroup;
  }

  /**
   * Determine whether input [source] string is a 
   * group declaration.
   */
  bool _isGroup(String source) {
    // is it a group line?
    if(group_re.hasMatch(source)){
      if(group_re.stringMatch(source) != null){
        return true;
      }
    }
    
    return false;
  }
  
  String _getGroupDeclaration(String source){
    return group_re.firstMatch(source)[1];
  }
  
  /**
   * Determine whether input [source] string is a 
   * comment.
   */
  bool _isComment(String source){
    // line is a comment?
    if(comment_re.hasMatch(source)){
      if(comment_re.stringMatch(source) != null){
        return true;
      }
    }
    
    return false;
  }
  
  /**
   * Determine whether input [source] string is a 
   * key-value.
   */
  bool _isKeyValue(String source) {
    
    // line is key-value?
    if(key_value_re.hasMatch(source)){
      if(key_value_re.stringMatch(source) != null){
        return true;
      }
    }
    
    return false;
  }
  

  /**
   * Collect the errors found while parsing current
   * TOML source, eventually throwing an exception if
   * exceptions are enabled.
   */
  _error(String message) {
    if(!_skipExceptions){
      throw new UnsupportedFormatException(message);
    }
    
    _errors.add(message);
  }
}

/**
 * An exception to be thrown in case of not supported
 * format is found on input source.
 */
class UnsupportedFormatException {
  
  String _message;
  
  UnsupportedFormatException(this._message);
}